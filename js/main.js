
var videos = document.querySelectorAll('.video'),
    videoBoxes = document.querySelectorAll('.videobox'),
    playBtn = document.getElementById('play-btn'),
    stopBtn = document.getElementById('stop-btn'),
    timeBtn = document.getElementById('time-btn'),
    vue = document.getElementById('vue'),
    body = document.body,
    context = new AudioContext(),
    bpm = 85,
    stepTime = 60 / bpm ,
    clock = 0,
    running = false,
    debug = true,
    metronome = false,
    trackPeriod = 30,
    currentlyPlaying = [],
    autoStart = true,
    autoPlayDelay = 10; // in seconds

if (!debug) {
  metronome = false;
}else {
  document.body.classList.add('debug')
}
// autoplay if not running yet

function launchAutoStart(){
  setTimeout(function(){
    if (!running && autoStart) {
      console.log('autostart');
      run()
    }
  }, autoPlayDelay * 1000)
}
launchAutoStart()

playBtn.addEventListener('click', event => {run()});
stopBtn.addEventListener('click', event => {stopAll()});
timeBtn.addEventListener('click', event => {getTime()});

pickPulse();

var id=0;
loadVideos = setInterval(function(){
  shiftVideo(id)
  setVolume(id)
  setId(id)
  getTimeSig(id)
  id++;
}, 100);


function shiftVideo(id){
  if (id >= videos.length -1) {
    clearInterval(loadVideos)
  }else {
    var video = videos[id];
    offset = video.getAttribute('data-offset');
    video.currentTime = offset;
    loadOffset(videoBoxes[id], offset)
  }
}

function getTimeSig(id){
  var sig = Number(videos[id].getAttribute('data-sig'));
  var sigTag = videos[id].parentNode.querySelectorAll('.sig')[0];
  sigTag.innerHTML = sig;
}
function setVolume(id){
  if (id <  videos.length) {
    var video = videos[id];
    volume = video.getAttribute('data-vol');
    video.volume = volume;
  }
}
function setId(id){
  if (id <  videos.length) {
    var video = videos[id];
    var videoBox = video.parentNode;
    var id = videoBox.id;
    var n = videoBox.querySelectorAll('.name')[0];
    n.innerHTML = id;
  }
}
function run(){
  loadVideos;
  tic()
  ticLoop = setInterval(function(){
    tic()
  }, stepTime * 1000);
  running = true;
  connectAll();
}

function tic(){
  nextTime();
  timeSig(currentlyPlaying);
  body.classList.add('tic')
  setTimeout(function(){
    body.classList.remove('tic')
  }, 100);
  if (metronome) {
    synth.triggerAttackRelease("C4", "4n");
  }
}

function getTime(){
  for (var i = 0; i < videoBoxes.length; i++) {
    var t = videoBoxes[i].querySelectorAll('.offset')[0];
    var v = videoBoxes[i].querySelectorAll('.video')[0];
    t.innerHTML = "▼ "+precise(v.currentTime);
  }
}
function loadOffset(videoBox, startTime){
  var l = videoBox.querySelectorAll('.load')[0];
  l.innerHTML = "loaded at "+startTime;
}
function stopAll() {
  clearInterval(ticLoop);
  for (var i = 0; i < videos.length; i++) {
    stopVideo(videos[i])
  }
  clock = 0;
  document.getElementById('clock').innerText = "";
  running = false;
  launchAutoStart()
}
function pickVideo(){
  var rv = Math.floor(Math.random() * videos.length);
  var pickedVideo = videos[rv];
  var videoBox = pickedVideo.parentNode;
  offset = pickedVideo.getAttribute('data-offset');
  pickedVideo.currentTime = offset;
  loadOffset(videoBox, offset)
  pickedVideo.play();
  pickedVideo.setAttribute('data-start', clock)
  videoBox.querySelectorAll('.start')[0].innerHTML = clock;
  currentlyPlaying.push(pickedVideo)
  videoBox.classList.add('playing')
  console.log(videoBox.id+" picked");
  videoBox.querySelectorAll('.load')[0].innerHTML = offset;
  pickPulse();
  console.log(currentlyPlaying);
}

function nextTime(){
  checkVideoStop()
  clock++;
  if(clock == nextPulse || clock == 1 ){
    pickVideo()
  }
  document.getElementById('clock').innerText = clock;
}

function pickPulse(){
  pickedPulse = trackPeriod;
  nextPulse = pickedPulse + clock;
  console.log('picked pulse: '+pickedPulse+" (next pulse: "+nextPulse+")");
}

for (var i = 0; i < videos.length; i++) {
  videos[i].onended = function(){
    stopVideo(this);
  }
}

function checkVideoStop(){
  for (var i = 0; i < currentlyPlaying.length; i++) {
    var currentVideo = currentlyPlaying[i];
    var videoBox = currentVideo.parentNode;
    var startTime = currentlyPlaying[i].getAttribute('data-start');
    var duration = currentlyPlaying[i].getAttribute('data-dur');
    var endTime = Number(startTime) + Number(duration);
    // if (clock == endTime && currentlyPlaying.includes(currentVideo)) {
    //    stopVideo(currentVideo, endTime)
    // }
  }
}
function timeSig(videos){
  for (var i = 0; i < videos.length; i++) {
    var sig = Number(videos[i].getAttribute('data-sig'));
    var videoBox = videos[i].parentNode;
    var sigTag = videoBox.querySelectorAll('.sig')[0];
    if (sigTag.innerHTML >= sig) {
      sigTag.innerHTML = 1;
    }else {
      sigTag.innerHTML = Number(sigTag.innerHTML) + 1;
    }
  }

}
function stopVideo(video, endTime){
  var videoBox = video.parentNode;
  var sig = Number(video.getAttribute('data-sig'));
  var sigTag = videoBox.querySelectorAll('.sig')[0];
  sigTag.innerHTML = sig;
  // find and remove the video from the currentlyPlaying array
  var v = currentlyPlaying.indexOf(video);
  if(v != -1) {
  	currentlyPlaying.splice(v, 1);
  }
  video.load();
  videoBox.classList.remove('playing');
  console.log(videoBox.id+" ended at "+endTime);
}

// Synth for the tic
var synth = new Tone.Synth({
	"oscillator" : {
		"type" : "sine",
		"modulationFrequency" : 0.2,
    "volume": 0, // -10 or 0
	},
	"envelope" : {
		"attack" : 0.01,
		"decay" : 0.1,
		"sustain" : 0.00001,
		"release" : 0.001,
    "attackCurve":'exponential',
	}
}).toMaster();

// Spacebar triggers play or stop
document.addEventListener('keydown', (event) => {
  const keyName = event.key;
  if (keyName === ' ') {
    if (running) { stopAll(); }
    else { run();}
  }
}, false);

function precise(x) { // Used as round()
  return Number.parseFloat(x).toPrecision(2);
}

// Write bpm and step time in the html
document.getElementById('bpm').innerText = bpm+"bpm";
document.getElementById('step').innerText = precise(stepTime)+"s";

window.onresize = function(event) {
  lines = document.querySelectorAll('.line')
  console.log('resize');
  unconnectAll(lines)
  connectAll()
}

// function removeDummy() {
//  var elem = document.getElementById('dummy');
//  elem.parentNode.removeChild(elem);
// }

function connectAll(){
  abconnect(document.getElementById('don'),document.getElementById('donda'));
  abconnect(document.getElementById('donda'),document.getElementById('dondadi'));
  abconnect(document.getElementById('dondadi'),document.getElementById('dondadifo'));
  abconnect(document.getElementById('dondadifo'),document.getElementById('donfotadimba'));
  abconnect(document.getElementById('donfotadimba'),document.getElementById('donfota'));
  abconnect(document.getElementById('donfota'),document.getElementById('donfo'));
  abconnect(document.getElementById('donfota'),document.getElementById('donf'));
  abconnect(document.getElementById('donfo'),document.getElementById('donfu'));
  abconnect(document.getElementById('donfu'),document.getElementById('donfi'));
  abconnect(document.getElementById('donfi'),document.getElementById('donf'));
  abconnect(document.getElementById('donf'),document.getElementById('don'));
}

function unconnectAll(lines){
  console.log(lines.length);
  for (var i = 0; i < lines.length; i++) {
    console.log('removing'+lines[i]);
    lines[i].parentNode.removeChild(lines[i]);
  }
}
