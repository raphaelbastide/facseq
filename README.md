# FacSeq Player (Alpha)

This app is part of the piece *FacSeq Player (Alpha)* created by Raphaël Bastide in 2019 for the exhibition *Please Trespass* shown in Paris at 19 Côté Cours, curated by Polynomes.
The app randomly plays video and sound sequences of performed musical instruments. Those instruments all inherit from a common parent *DON*, a one-meter long aluminum tube. From this tube were crafted flutes, chimes, idiophones, and other percussion instruments. The whole group of objects forms a genealogy of instruments inspired by the nonlinear timelines used in version control systems of digital content such as Git.

## License

[GPLv3](https://www.gnu.org/licenses/gpl.html)

## Thx

Documented with the help of Albin Metthey and Louise Drulhe.

## Web page

https://raphaelbastide.com/facseq/

## Images

![Player](images/facseq-player-alpha.png)

![Installation view](images/installation-view.jpg)

![DONFOTADIMBA being played](images/donfotadimba.png)
